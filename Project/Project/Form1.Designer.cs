﻿namespace Project
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.AnT = new Tao.Platform.Windows.SimpleOpenGlControl();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.HButton = new System.Windows.Forms.ToolStripMenuItem();
            this.Render = new System.Windows.Forms.Timer(this.components);
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label5 = new System.Windows.Forms.Label();
            this.Color = new System.Windows.Forms.ComboBox();
            this.PauseButton = new System.Windows.Forms.Button();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.IsVal = new System.Windows.Forms.Label();
            this.IfVal = new System.Windows.Forms.Label();
            this.Ifin = new System.Windows.Forms.Label();
            this.Istart = new System.Windows.Forms.Label();
            this.Alf1_val = new System.Windows.Forms.Label();
            this.Alf2_val = new System.Windows.Forms.Label();
            this.Quality = new System.Windows.Forms.TrackBar();
            this.FirstA = new System.Windows.Forms.TrackBar();
            this.pol_val = new System.Windows.Forms.Label();
            this.SecondA = new System.Windows.Forms.TrackBar();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.wave_val = new System.Windows.Forms.Label();
            this.trackBar1 = new System.Windows.Forms.TrackBar();
            this.Polarize = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.menuStrip1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Quality)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FirstA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SecondA)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
            this.tabControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // AnT
            // 
            this.AnT.AccumBits = ((byte)(0));
            this.AnT.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.AnT.AutoCheckErrors = false;
            this.AnT.AutoFinish = false;
            this.AnT.AutoMakeCurrent = true;
            this.AnT.AutoSwapBuffers = true;
            this.AnT.BackColor = System.Drawing.Color.Black;
            this.AnT.ColorBits = ((byte)(32));
            this.AnT.DepthBits = ((byte)(16));
            this.AnT.Location = new System.Drawing.Point(12, 27);
            this.AnT.Name = "AnT";
            this.AnT.Size = new System.Drawing.Size(544, 423);
            this.AnT.StencilBits = ((byte)(0));
            this.AnT.TabIndex = 0;
            this.AnT.SizeChanged += new System.EventHandler(this.AnT_SizeChanged);
            this.AnT.MouseDown += new System.Windows.Forms.MouseEventHandler(this.AnT_MouseDown);
            this.AnT.MouseMove += new System.Windows.Forms.MouseEventHandler(this.AnT_MouseMove);
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.HButton});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(784, 24);
            this.menuStrip1.TabIndex = 7;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // HButton
            // 
            this.HButton.Name = "HButton";
            this.HButton.Size = new System.Drawing.Size(44, 20);
            this.HButton.Text = "Help";
            this.HButton.Click += new System.EventHandler(this.HButton_Click);
            // 
            // Render
            // 
            this.Render.Interval = 17;
            this.Render.Tick += new System.EventHandler(this.Render_Tick);
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.Lavender;
            this.tabPage2.Controls.Add(this.label5);
            this.tabPage2.Controls.Add(this.Color);
            this.tabPage2.Controls.Add(this.PauseButton);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(214, 257);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Визуализация";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Освещение:";
            // 
            // Color
            // 
            this.Color.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Color.FormattingEnabled = true;
            this.Color.Items.AddRange(new object[] {
            "Вкл.",
            "Выкл."});
            this.Color.Location = new System.Drawing.Point(95, 6);
            this.Color.Name = "Color";
            this.Color.Size = new System.Drawing.Size(113, 21);
            this.Color.TabIndex = 8;
            // 
            // PauseButton
            // 
            this.PauseButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.PauseButton.Location = new System.Drawing.Point(133, 33);
            this.PauseButton.Name = "PauseButton";
            this.PauseButton.Size = new System.Drawing.Size(75, 23);
            this.PauseButton.TabIndex = 9;
            this.PauseButton.Text = "Pause";
            this.PauseButton.UseVisualStyleBackColor = true;
            this.PauseButton.Click += new System.EventHandler(this.PauseButton_Click);
            // 
            // tabPage1
            // 
            this.tabPage1.AutoScroll = true;
            this.tabPage1.BackColor = System.Drawing.Color.Lavender;
            this.tabPage1.Controls.Add(this.IsVal);
            this.tabPage1.Controls.Add(this.IfVal);
            this.tabPage1.Controls.Add(this.Ifin);
            this.tabPage1.Controls.Add(this.Istart);
            this.tabPage1.Controls.Add(this.Alf1_val);
            this.tabPage1.Controls.Add(this.Alf2_val);
            this.tabPage1.Controls.Add(this.Quality);
            this.tabPage1.Controls.Add(this.FirstA);
            this.tabPage1.Controls.Add(this.pol_val);
            this.tabPage1.Controls.Add(this.SecondA);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(214, 257);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Физика";
            // 
            // IsVal
            // 
            this.IsVal.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.IsVal.Location = new System.Drawing.Point(153, 315);
            this.IsVal.Name = "IsVal";
            this.IsVal.Size = new System.Drawing.Size(38, 23);
            this.IsVal.TabIndex = 16;
            this.IsVal.Text = "1";
            this.IsVal.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // IfVal
            // 
            this.IfVal.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.IfVal.Location = new System.Drawing.Point(153, 349);
            this.IfVal.Name = "IfVal";
            this.IfVal.Size = new System.Drawing.Size(38, 23);
            this.IfVal.TabIndex = 15;
            this.IfVal.Text = "0.5";
            this.IfVal.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Ifin
            // 
            this.Ifin.AutoSize = true;
            this.Ifin.Location = new System.Drawing.Point(3, 349);
            this.Ifin.Name = "Ifin";
            this.Ifin.Size = new System.Drawing.Size(120, 26);
            this.Ifin.TabIndex = 14;
            this.Ifin.Text = "Интенсивность света,\r\nпадающего на экран:";
            // 
            // Istart
            // 
            this.Istart.AutoSize = true;
            this.Istart.Location = new System.Drawing.Point(3, 315);
            this.Istart.Name = "Istart";
            this.Istart.Size = new System.Drawing.Size(153, 26);
            this.Istart.TabIndex = 13;
            this.Istart.Text = "Интенсивность излучаемого\r\nсвета:";
            // 
            // Alf1_val
            // 
            this.Alf1_val.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Alf1_val.Location = new System.Drawing.Point(163, 203);
            this.Alf1_val.Name = "Alf1_val";
            this.Alf1_val.Size = new System.Drawing.Size(28, 23);
            this.Alf1_val.TabIndex = 8;
            this.Alf1_val.Text = "0";
            this.Alf1_val.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Alf2_val
            // 
            this.Alf2_val.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Alf2_val.Location = new System.Drawing.Point(163, 267);
            this.Alf2_val.Name = "Alf2_val";
            this.Alf2_val.Size = new System.Drawing.Size(28, 23);
            this.Alf2_val.TabIndex = 9;
            this.Alf2_val.Text = "0";
            this.Alf2_val.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Quality
            // 
            this.Quality.Location = new System.Drawing.Point(6, 129);
            this.Quality.Maximum = 100;
            this.Quality.Minimum = 50;
            this.Quality.Name = "Quality";
            this.Quality.Size = new System.Drawing.Size(151, 45);
            this.Quality.TabIndex = 11;
            this.Quality.Value = 100;
            this.Quality.ValueChanged += new System.EventHandler(this.Quality_ValueChanged);
            // 
            // FirstA
            // 
            this.FirstA.Location = new System.Drawing.Point(6, 203);
            this.FirstA.Maximum = 360;
            this.FirstA.Name = "FirstA";
            this.FirstA.Size = new System.Drawing.Size(151, 45);
            this.FirstA.TabIndex = 10;
            this.FirstA.ValueChanged += new System.EventHandler(this.FirstA_ValueChanged);
            // 
            // pol_val
            // 
            this.pol_val.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pol_val.Location = new System.Drawing.Point(163, 129);
            this.pol_val.Name = "pol_val";
            this.pol_val.Size = new System.Drawing.Size(28, 23);
            this.pol_val.TabIndex = 7;
            this.pol_val.Text = "100";
            this.pol_val.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // SecondA
            // 
            this.SecondA.Location = new System.Drawing.Point(6, 267);
            this.SecondA.Maximum = 360;
            this.SecondA.Name = "SecondA";
            this.SecondA.Size = new System.Drawing.Size(151, 45);
            this.SecondA.TabIndex = 9;
            this.SecondA.ValueChanged += new System.EventHandler(this.SecondA_ValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 251);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(135, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Угол 2-ого поляризатора";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 187);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(135, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Угол 1-ого поляризатора";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.wave_val);
            this.groupBox1.Controls.Add(this.trackBar1);
            this.groupBox1.Controls.Add(this.Polarize);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(6, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(188, 104);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Свойства света";
            // 
            // wave_val
            // 
            this.wave_val.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.wave_val.Location = new System.Drawing.Point(157, 32);
            this.wave_val.Name = "wave_val";
            this.wave_val.Size = new System.Drawing.Size(28, 19);
            this.wave_val.TabIndex = 6;
            this.wave_val.Text = "500";
            this.wave_val.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // trackBar1
            // 
            this.trackBar1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.trackBar1.Location = new System.Drawing.Point(3, 32);
            this.trackBar1.Maximum = 740;
            this.trackBar1.Minimum = 380;
            this.trackBar1.Name = "trackBar1";
            this.trackBar1.Size = new System.Drawing.Size(148, 45);
            this.trackBar1.TabIndex = 1;
            this.trackBar1.Value = 500;
            this.trackBar1.ValueChanged += new System.EventHandler(this.trackBar1_ValueChanged);
            // 
            // Polarize
            // 
            this.Polarize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Polarize.AutoSize = true;
            this.Polarize.Location = new System.Drawing.Point(65, 83);
            this.Polarize.Name = "Polarize";
            this.Polarize.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Polarize.Size = new System.Drawing.Size(117, 17);
            this.Polarize.TabIndex = 5;
            this.Polarize.Text = ":Поляризованный";
            this.Polarize.UseVisualStyleBackColor = true;
            this.Polarize.CheckedChanged += new System.EventHandler(this.Polarize_CheckedChanged);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Длина волны (нм):";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 113);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(170, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Качество поляроида (50 - 100%):";
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(562, 27);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(222, 283);
            this.tabControl1.TabIndex = 17;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.Image = global::Project.Properties.Resources.NSTU_Logo;
            this.pictureBox1.Location = new System.Drawing.Point(562, 316);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(210, 134);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 17;
            this.pictureBox1.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ClientSize = new System.Drawing.Size(784, 462);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.AnT);
            this.Controls.Add(this.menuStrip1);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Виртуальная лаборатория: Опыт Малюса";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Quality)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FirstA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SecondA)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            this.tabControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Tao.Platform.Windows.SimpleOpenGlControl AnT;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem HButton;
        private System.Windows.Forms.Timer Render;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.ComboBox Color;
        private System.Windows.Forms.Button PauseButton;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TrackBar Quality;
        private System.Windows.Forms.TrackBar FirstA;
        private System.Windows.Forms.TrackBar SecondA;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TrackBar trackBar1;
        private System.Windows.Forms.CheckBox Polarize;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label Alf1_val;
        private System.Windows.Forms.Label Alf2_val;
        private System.Windows.Forms.Label pol_val;
        private System.Windows.Forms.Label wave_val;
        private System.Windows.Forms.Label Ifin;
        private System.Windows.Forms.Label Istart;
        private System.Windows.Forms.Label IsVal;
        private System.Windows.Forms.Label IfVal;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}

