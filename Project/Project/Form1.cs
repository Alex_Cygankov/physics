﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Windows.Forms;
// для работы с библиотекой OpenGL 
using Tao.OpenGl;
// для работы с библиотекой FreeGLUT 
using Tao.FreeGlut;
// для работы с элементом управления SimpleOpenGLControl 
using Tao.Platform.Windows;
// для работы с библиотекой DevIl 
using Tao.DevIl;


namespace Project
{
    public partial class Form1 : Form
    {
        // отсчет времени 
        float global_time = 0;
        bool pause = false;
        float X, Y, Z, dx = 0,
            fI, sI, PolI, T, R, fpol, spol,
            Alfa, Beta;

        float[] MainColour;
        float[] light_position;


        // генератор случайны чисел 
        Random rnd = new Random();

        anModelLoader FPolarA, FPolarB, Screen,
            SPolarA, SPolarB,
            Lantern, Bench;

        TexturesForObjects Table;

        public Form1()
        {
            InitializeComponent();
            AnT.InitializeContexts();
        }

        private void converter()
        {
            int value = trackBar1.Value - 380;
            if (0 <= value && value <= 29)			//от прозрачного фиолетового до непрозрачного фиолетового
            {
                MainColour[0] = 0.5f;
                MainColour[1] = 0;
                MainColour[2] = 0.5f;
                MainColour[3] = (float)value / 29.0f;
            }
            else if (30 <= value && value <= 82)	//от фиолетового до синего
            {
                MainColour[0] = 0.5f - 0.5f * (float)(value - 30) / 52.0f;
                MainColour[1] = 0;
                MainColour[2] = 0.5f + 0.5f * (float)(value - 30) / 52.0f;
                MainColour[3] = 1.0f;
            }
            else if (83 <= value && value <= 112)	//от синего до голубого
            {
                MainColour[0] = 0.5f * (float)(value - 83) / 29.0f;
                MainColour[1] = 0.5f * (float)(value - 83) / 29.0f;
                MainColour[2] = 1.0f;
                MainColour[3] = 1.0f;
            }
            else if (113 <= value && value <= 152)	//от голубого до зеленого
            {
                MainColour[0] = 0.5f - 0.5f * (float)(value - 113) / 39.0f;
                MainColour[1] = 0.5f + 0.5f * (float)(value - 113) / 39.0f;
                MainColour[2] = 1.0f - 1f * (float)(value - 113) / 39.0f;
                MainColour[3] = 1.0f;
            }
            else if (153 <= value && value <= 202)	//от зеленого до желтого
            {
                MainColour[0] = 0.5f * (float)(value - 153) / 49.0f;
                MainColour[1] = 1.0f-0.5f * (float)(value - 153) / 49.0f;
                MainColour[2] = 0;
                MainColour[3] = 1.0f;
            }
            else if (203 <= value && value <= 237)	//от желтого до оранжевого
            {
                MainColour[0] = 0.5f + 0.5f * (float)(value - 203) / 34.0f;
                MainColour[1] = 0.5f;
                MainColour[2] = 0;
                MainColour[3] = 1.0f;
            }
            else if (238 <= value && value <= 302)	//от оранжевого до красного
            {
                MainColour[0] = 1;
                MainColour[1] = 0.5f * ((float)(302 - value) / 64.0f);
                MainColour[2] = 0;
                MainColour[3] = 1.0f;
            }
            else if (303 <= value && value <= 360)	//от непрозрачного красного до прозрачного красного
            {
                MainColour[0] = 1.0f;
                MainColour[1] = 0;
                MainColour[2] = 0;
                MainColour[3] = 1.0f * (360 - value) / 56.0f;
            }
            MainColour[0] = Math.Abs(MainColour[0]);
            MainColour[1] = Math.Abs(MainColour[1]);
            MainColour[2] = Math.Abs(MainColour[2]);
            MainColour[3] = Math.Abs(MainColour[3]);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //инициаллизация пути до файла справки
            string fileNameExe = Assembly.GetExecutingAssembly().Location;
            string fileNameHlp = Path.ChangeExtension(fileNameExe, "chm");
            MainColour = new float[4];
            light_position = new float[3] { 0, 10, -10 };
            Alfa = 0;
            Beta = (float)Math.PI / 1.1f;
            R = 15;
            Y = (float)Math.Sqrt(1.5f);
            dx = 3.5f;
            T = (float)trackBar1.Value / 740;
            fI = (float)Math.Sqrt(1.5f);
            sI = (float)Math.Sqrt(1.5f);
            PolI = (float)Math.Sqrt(1.5f);
            Color.SelectedIndex = 0;
            MouseWheel += new MouseEventHandler(this.Priblijalka);
            // инициализация OpenGL 
            MainColour[0] = 0;
            MainColour[1] = 1.0f;
            MainColour[2] = 1.0f;
            MainColour[3] = 1;
            Glut.glutInit();
            Il.ilInit();
            Glut.glutInitDisplayMode(Glut.GLUT_RGBA | Glut.GLUT_DOUBLE | Glut.GLUT_DEPTH);
            Il.ilEnable(Il.IL_ORIGIN_SET);

            Gl.glClearColor(1f, 1f, 1f, 1f);

            Gl.glViewport(0, 0, AnT.Width, AnT.Height);

            Gl.glMatrixMode(Gl.GL_PROJECTION);

            Glu.gluPerspective(45, (float)AnT.Width / (float)AnT.Height, 0.1, 200);

            Gl.glMatrixMode(Gl.GL_MODELVIEW);
            Gl.glLoadIdentity();

            Gl.glLightfv(Gl.GL_LIGHT0, Gl.GL_POSITION, light_position);
            Gl.glEnable(Gl.GL_DEPTH_TEST);
            Gl.glEnable(Gl.GL_COLOR_MATERIAL);
            Gl.glEnable(Gl.GL_LIGHTING);
            Gl.glEnable(Gl.GL_LIGHT0);

            //загрузка моделей
            FPolarA = new anModelLoader();
            FPolarA.LoadModel("models/RingA.ASE");

            FPolarB = new anModelLoader();
            FPolarB.LoadModel("models/RingB.ASE");

            SPolarB = new anModelLoader();
            SPolarB.LoadModel("models/RingB.ASE");

            SPolarA = new anModelLoader();
            SPolarA.LoadModel("models/RingA.ASE");

            Lantern = new anModelLoader();
            Lantern.LoadModel("models/Lantern.ASE");

            Bench = new anModelLoader();
            Bench.LoadModel("models/Bench.ASE");

            Screen = new anModelLoader();
            Screen.LoadModel("models/Screen.ASE");

            Table = new TexturesForObjects();
            Table.LoadTextureForModel("models\\wood.jpg");

            converter();

            // активация таймера 
            Render.Start();
        }

        private void Draw()
        {
            Gl.glEnable(Gl.GL_BLEND);
            Gl.glBlendFunc(Gl.GL_SRC_ALPHA, Gl.GL_ONE_MINUS_SRC_ALPHA);
            // Сглаживание точек
            Gl.glEnable(Gl.GL_POINT_SMOOTH);
            Gl.glHint(Gl.GL_POINT_SMOOTH_HINT, Gl.GL_NICEST);
            // Сглаживание линий
            Gl.glEnable(Gl.GL_LINE_SMOOTH);
            Gl.glHint(Gl.GL_LINE_SMOOTH_HINT, Gl.GL_NICEST);
            // Сглаживание полигонов    
            Gl.glEnable(Gl.GL_POLYGON_SMOOTH);
            Gl.glHint(Gl.GL_POLYGON_SMOOTH_HINT, Gl.GL_NICEST);

            // в зависимсоти от установленног оредима отрисовываем сцену в черном или белом цвете 
            if (Color.SelectedIndex == 0)
            {
                // цвет очистки окна 
                Gl.glClearColor(1, 1, 1, 0);
            }
            else
            {
                Gl.glClearColor(0, 0, 0, 0);
            }

            Gl.glClear(Gl.GL_COLOR_BUFFER_BIT | Gl.GL_DEPTH_BUFFER_BIT);

            Gl.glLoadIdentity();


            // цвет рисования 
            Gl.glColor4f(MainColour[0], MainColour[1], MainColour[2], 1);

            Gl.glPushMatrix();

            // Положение камеры
            Glu.gluLookAt(dx + R * Math.Sin(Alfa) * Math.Cos(Beta), R * Math.Sin(Beta), -R * Math.Cos(Alfa) * Math.Cos(Beta), dx, 1, 0, 0, 1, 0);


            Gl.glPushMatrix();

            Gl.glTranslatef(-24.2f, 5.68f, 0);
            Gl.glRotatef(90f, 0, 0, 1);
            Glut.glutSolidSphere(1.51f, 2, 32);
            Gl.glRotatef(-90f, 0, 0, 1);
            Gl.glTranslatef(24.2f, -5.68f, 0);

            //отрисовка импортированных моделей
            Gl.glTranslatef(-25.0f, 0, -2.0f);
            Gl.glRotatef(-90.0f, 0, 1, 0);
            Gl.glRotatef(-90.0f, 1, 0, 0);
            Bench.DrawModel();
            Lantern.DrawModel();
            Gl.glRotatef(90.0f, 0, 0, 1);
            Gl.glTranslatef(18.6875f, 0, 0);
            Gl.glRotatef(-90.0f, 0, 0, 1);
            Gl.glTranslatef(1.96f, 0, 5.69f);
            Gl.glRotatef((float)FirstA.Value, 0, 1, 0);
            Gl.glTranslatef(-1.96f, 0, -5.69f);
            FPolarA.DrawModel();
            Gl.glTranslatef(1.96f, 0, 5.69f);
            Gl.glRotatef(-(float)FirstA.Value, 0, 1, 0);
            Gl.glTranslatef(-1.96f, 0, -5.69f);
            FPolarB.DrawModel();
            Gl.glRotatef(90.0f, 0, 0, 1);
            Gl.glTranslatef(18.6875f, 0, 0);
            Gl.glRotatef(-90.0f, 0, 0, 1);
            Gl.glTranslatef(1.96f, 0, 5.69f);
            Gl.glRotatef((float)SecondA.Value, 0, 1, 0);
            Gl.glTranslatef(-1.96f, 0, -5.69f);
            SPolarA.DrawModel();
            Gl.glTranslatef(1.96f, 0, 5.69f);
            Gl.glRotatef(-(float)SecondA.Value, 0, 1, 0);
            Gl.glTranslatef(-1.96f, 0, -5.69f);
            SPolarB.DrawModel();
            Gl.glRotatef(90.0f, 0, 0, 1);
            Gl.glTranslatef(18.548f, 0, 0);
            Gl.glRotatef(-90.0f, 0, 0, 1);
            Screen.DrawModel();

            Gl.glPopMatrix();

            //отрисовка примитивов
            DrawPrimitive();



            Gl.glPopMatrix();
            Gl.glFlush();

            Gl.glDisable(Gl.GL_BLEND);
            // Сглаживание точек
            Gl.glDisable(Gl.GL_POINT_SMOOTH);
            // Сглаживание линий
            Gl.glDisable(Gl.GL_LINE_SMOOTH);
            // Сглаживание полигонов    
            Gl.glDisable(Gl.GL_POLYGON_SMOOTH);
            // обновляем окно 
            AnT.Invalidate();
        }

        // функция для отрисовки матрицы 
        private void DrawPrimitive()
        {
            // включаем режим текстурирования 
            Gl.glEnable(Gl.GL_TEXTURE_2D);
            Gl.glTexParameteri(Gl.GL_TEXTURE_2D, Gl.GL_TEXTURE_MIN_FILTER, Gl.GL_LINEAR);
            Gl.glTexParameteri(Gl.GL_TEXTURE_2D, Gl.GL_TEXTURE_MAG_FILTER, Gl.GL_LINEAR);
            // включаем режим текстурирования, указывая идентификатор mGlTextureObject 
            Gl.glBindTexture(Gl.GL_TEXTURE_2D, Table.GetTextureObj());
            // отрисовываем полигон 
            Gl.glBegin(Gl.GL_QUADS);

            // указываем поочередно текстурные координаты и вершины 
            Gl.glTexCoord2f(0, 0.5f);
            Gl.glVertex3d(-40, 0, -20);
            Gl.glTexCoord2f(1f, 0.5f);
            Gl.glVertex3d(-40, 0, 20);
            Gl.glTexCoord2f(1f, 0);
            Gl.glVertex3d(40, 0, 20);
            Gl.glTexCoord2f(0, 0);
            Gl.glVertex3d(40, 0, -20);

            Gl.glVertex3d(40, 0, -20);
            Gl.glVertex3d(40, -0.5f, -20);
            Gl.glVertex3d(40, -0.5f, 20);
            Gl.glVertex3d(40, 0, 20);

            Gl.glVertex3d(-40, 0, -20);
            Gl.glVertex3d(40, 0, -20);
            Gl.glVertex3d(40, -0.5f, -20);
            Gl.glVertex3d(-40, -0.5f, -20);

            Gl.glVertex3d(40, 0, 20);
            Gl.glVertex3d(-40, 0, 20);
            Gl.glVertex3d(-40, -0.5f, 20);
            Gl.glVertex3d(40, -0.5f, 20);

            Gl.glVertex3d(-40, 0, -20);
            Gl.glVertex3d(-40, 0, 20);
            Gl.glVertex3d(-40, -0.5f, 20);
            Gl.glVertex3d(-40, -0.5f, -20);

            // завершаем отрисовку 
            Gl.glEnd();

            // отключаем режим текстурирования 
            Gl.glDisable(Gl.GL_TEXTURE_2D);

           
            Gl.glDisable(Gl.GL_LIGHTING);
            Gl.glDisable(Gl.GL_LIGHT0);
            Gl.glBegin(Gl.GL_LINES);

            Gl.glColor4f(0.005f, 0.005f, 0.005f, 1);
            Gl.glVertex3d(-25, 5.68f, 0);
            Gl.glVertex3d(31.3f, 5.68f, 0);
            Gl.glColor4f(MainColour[0], MainColour[1], MainColour[2], MainColour[3]);
            Gl.glRotated(-(float)Math.PI / 2, 1, 0, 0);
            if (Polarize.Checked)
            {
                for (int i = 150; i < 300; i++)// свет после первого поляризатора
                {
                    Gl.glVertex3d(i * 0.125f - 25, 5.68f, 0);
                    Gl.glVertex3d(i * 0.125f - 25,
                        5.68f + fI * Math.Sin(0.1f * i / T - global_time) *
                        Math.Cos((float)FirstA.Value / 180 * Math.PI)* Math.Pow(-1, (90 + FirstA.Value) / 180),
                        fI * Math.Sin(0.1f * i / T - global_time) *
                        Math.Sin((float)FirstA.Value / 180 * Math.PI) * Math.Pow(-1, (90 + FirstA.Value) / 180));
                }
                for (int i = 300; i < 450; i++)// свет после второго поляризатора
                {
                    Gl.glVertex3d(i * 0.125f - 25, 5.68f, 0);
                    Gl.glVertex3d(i * 0.125f - 25,
                        5.68f + sI * Math.Sin(0.1f * i / T - global_time) * Math.Pow(-1, (90 + FirstA.Value) / 180) *
                        Math.Cos((float)SecondA.Value / 180 * Math.PI) * Math.Pow(-1, (90 + Math.Abs(SecondA.Value - FirstA.Value)) / 180),
                        sI * Math.Sin(0.1f * i / T - global_time) * Math.Sin((float)SecondA.Value / 180 * Math.PI) *
                        Math.Pow(-1, (90 + Math.Abs(SecondA.Value - FirstA.Value)) / 180) * Math.Pow(-1, (90 + FirstA.Value) / 180));
                }
                Gl.glVertex3d(31.25f, 5.68f - sI *
                    Math.Cos((float)SecondA.Value / 180 * Math.PI) * Math.Pow(-1, (90 + SecondA.Value) / 180),
                    -sI * Math.Sin((float)SecondA.Value / 180 * Math.PI) * Math.Pow(-1, (90 + SecondA.Value) / 180));
                Gl.glVertex3d(31.25f, 5.68f + sI *
                    Math.Cos((float)SecondA.Value / 180 * Math.PI) * Math.Pow(-1, (90 + SecondA.Value) / 180),
                    sI * Math.Sin((float)SecondA.Value / 180 * Math.PI) * Math.Pow(-1, (90 + SecondA.Value) / 180));
            }
            else
            {
                for (int i = 150; i < 300; i++)
                {
                    Gl.glVertex3d(i * 0.125f - 25, 5.68f, 0);
                    Gl.glVertex3d(i * 0.125f - 25,
                                5.68f + Y * Math.Sin(0.1f * i / T - global_time) *
                                Math.Cos((float)FirstA.Value / 180 * Math.PI),
                                Y * Math.Sin(0.1f * i / T - global_time) *
                        Math.Sin((float)FirstA.Value / 180 * Math.PI));
                }
                for (int i = 300; i < 450; i++)
                {
                    Gl.glVertex3d(i * 0.125f - 25, 5.68f, 0);
                    Gl.glVertex3d(i * 0.125f - 25,
                                5.68f + PolI * Math.Sin(0.1f * i / T - global_time) *
                                Math.Cos((float)SecondA.Value / 180 * Math.PI) * Math.Pow(-1, (90 + Math.Abs(SecondA.Value - FirstA.Value)) / 180),
                                PolI * Math.Sin(0.1f * i / T - global_time) * Math.Sin((float)SecondA.Value / 180 * Math.PI) *
                        Math.Pow(-1, (90 + Math.Abs(SecondA.Value - FirstA.Value)) / 180));
                }
                Gl.glVertex3d(31.25f,
                    5.68f - PolI * Math.Cos((float)SecondA.Value / 180 * Math.PI) * Math.Pow(-1, (90 + Math.Abs(SecondA.Value - FirstA.Value)) / 180),
                   -PolI * Math.Sin((float)SecondA.Value / 180 * Math.PI) * Math.Pow(-1, (90 + Math.Abs(SecondA.Value - FirstA.Value)) / 180));
                Gl.glVertex3d(31.25f,
                    5.68f + PolI * Math.Cos((float)SecondA.Value / 180 * Math.PI) * Math.Pow(-1, (90 + Math.Abs(SecondA.Value - FirstA.Value)) / 180),
                    PolI * Math.Sin((float)SecondA.Value / 180 * Math.PI) * Math.Pow(-1, (90 + Math.Abs(SecondA.Value - FirstA.Value)) / 180));
            }
            Gl.glRotated((float)Math.PI / 2, 1, 0, 0);
            Gl.glEnd();
            Gl.glEnable(Gl.GL_LIGHTING);
            Gl.glEnable(Gl.GL_LIGHT0);
            if (Polarize.Checked)
            {
                Gl.glDisable(Gl.GL_LIGHTING);
                Gl.glDisable(Gl.GL_LIGHT0);
                Gl.glBegin(Gl.GL_LINES);
                for (int i = 0; i < 150; i++)
                {
                    Gl.glVertex3d(i * 0.125f - 25, 5.68f, 0);
                    Gl.glVertex3d(i * 0.125f - 25, 5.68f + Math.Sqrt(1.5f) * Math.Sin(0.1f * i / T - global_time), 0);
                }
                Gl.glEnd();
                Gl.glEnable(Gl.GL_LIGHTING);
                Gl.glEnable(Gl.GL_LIGHT0);
            }
            else
            {
                Gl.glColor4f(MainColour[0], MainColour[1], MainColour[2], 0.3f * MainColour[3]);
                Gl.glPushMatrix();
                Gl.glTranslatef(-25.0f, 5.68f, 0);
                Gl.glRotated(90.0f, 0, 1, 0);
                Glut.glutSolidCylinder(1.5f, 19f, 32, 1);
                Gl.glRotated(-90.0f, 0, 1, 0);
                Gl.glTranslatef(25.0f, -5.68f, 0);
            }

            if (Polarize.Checked)
            {
                fpol = (float)Math.Abs(Math.Sin((float)(FirstA.Value) / 180 * Math.PI)) + (1f - (float)Quality.Value / 100);
                spol = (float)Math.Abs(Math.Cos((float)(SecondA.Value - FirstA.Value) / 180 * Math.PI)) * Quality.Value / 100;
            }
            else
            {
                fpol = 0;
                spol = (float)Math.Abs(Math.Cos((float)(SecondA.Value - FirstA.Value) / 180 * Math.PI)) * Quality.Value / 100;
            }
            Gl.glColor4f(spol, spol, spol, (1 - spol));
            Gl.glTranslated(12.4f, 5.68f, 0);
            Gl.glRotated(90f, 0, 1, 0);
            Glut.glutSolidCylinder(1.51f, 0.8f, 32, 1);
            Gl.glRotated(-90f, 0, 1, 0);
            Gl.glTranslated(-18.75f, 0, 0);
            Gl.glColor4f(1 - fpol, 1 - fpol, 1 - fpol, fpol);
            Gl.glRotated(90f, 0, 1, 0);
            Glut.glutSolidCylinder(1.51f, 0.8f, 32, 1);
            Gl.glRotated(-90f, 0, 1, 0);
            Gl.glTranslated(6.35f, -5.68f, 0.05f);

            Gl.glColor4f(MainColour[0], MainColour[1], MainColour[2], MainColour[3]);
            Gl.glPopMatrix();

            Gl.glLineWidth(2);
        }

        private void Render_Tick(object sender, EventArgs e)
        {
            if (!pause)
            // отсчитываем время 
            global_time += (float)Render.Interval / 1000;
            // вызываем функцию отрисовки 
            Draw();
        }

        private void Priblijalka(object sender, MouseEventArgs e)
        {
            if (e.Delta > 0 && R > 7)
            {
                R -= 0.5f;
            }
            if (e.Delta < 0 && R < 30)
            {
                R += 0.5f;
            }
            if (pause)
                Draw();
        }

        private void PauseButton_Click(object sender, EventArgs e)
        {
            pause = !pause;
        }

        private void AnT_SizeChanged(object sender, EventArgs e)
        {
            Gl.glViewport(0, 0, AnT.Width, AnT.Height);
            Glu.gluPerspective(45, (float)AnT.Width / (float)AnT.Height, 0.1, 200);
        }

        //Управление камерой
        private void AnT_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                if (Math.Abs(dx + (e.X - X) / 20) <= 25)
                    dx += (e.X - X) / 20;
                X = e.X;
                Z = e.Y;
            }
            if (e.Button == MouseButtons.Right)
            {

                if (e.X - X > 0 && Alfa < Math.PI / 7 * 3)
                    Alfa += (float)Math.PI / 360;
                else if (e.X - X != 0 && Alfa > -Math.PI / 7 * 3)
                    Alfa -= (float)Math.PI / 360;

                if (e.Y - Z < 0 && Beta < Math.PI / 1.1f)
                    Beta += (float)Math.PI / 360;
                else if (e.Y - Z != 0 && Beta > Math.PI * 181 / 360)
                    Beta -= (float)Math.PI / 360;
                Z = e.Y;
                X = e.X;
            }
        }


        private void AnT_MouseDown(object sender, MouseEventArgs e)
        {
            X = e.X;
            Z = e.Y;
        }

        //перерасчет амплитуд при изменении угла поворота 1-ого поляризатора
        private void FirstA_ValueChanged(object sender, EventArgs e)
        {
            // вывод текущего значения угла 1-ого поляризатора в форму
            Alf1_val.Text = Convert.ToString(FirstA.Value); 
            // вычисление амплитуды Е после прохода первого поляризатора при изначально поляризованном свете
            fI = (float)(Math.Sqrt((1.5f) * Quality.Value / 100) * Math.Abs(Math.Cos((float)FirstA.Value / 180 * Math.PI)));
            // вычисление амплитуды Е после прохода второго поляризатора при изначально поляризованном свете
            sI = (float)(fI * Math.Sqrt((float)Quality.Value / 100) * Math.Abs(Math.Cos((float)(SecondA.Value - FirstA.Value) / 180 * Math.PI)));
            // вычисление амплитуды Е после прохода второго поляризатора при изначально не поляризованном свете
            PolI = (float)(Y * Math.Abs(Math.Cos((float)(SecondA.Value - FirstA.Value) / 180 * Math.PI)) * Math.Sqrt((float)Quality.Value / 100));
            // вывод текущего амплитуды Е при попадании на экран в форму
            if (Polarize.Checked) 
                // если свет изначально поляризован
                IfVal.Text = Convert.ToString(Math.Round( sI*sI/1.5f, 2));
            else
                // если свет изначально неполяризован
                IfVal.Text = Convert.ToString(Math.Round(PolI * PolI / 3.0f, 2));
        }

        //перерасчет амплитуд при изменении угла поворота 2-ого поляризатора
        private void SecondA_ValueChanged(object sender, EventArgs e)
        {
            Alf2_val.Text = Convert.ToString(SecondA.Value);
            sI = (float)(fI * Math.Abs(Math.Cos((float)(SecondA.Value - FirstA.Value) / 180 * Math.PI)) * Math.Sqrt((float)Quality.Value / 100));
            PolI = (float)(Y * Math.Abs(Math.Cos((float)(SecondA.Value - FirstA.Value) / 180 * Math.PI)) * Math.Sqrt((float)Quality.Value / 100));
            if (Polarize.Checked)
                IfVal.Text = Convert.ToString(Math.Round(sI * sI / 1.5f, 2));
            else
                IfVal.Text = Convert.ToString(Math.Round(PolI * PolI / 3.0f, 2));
        }

        //перерасчет периода при изменении длины волны
        private void trackBar1_ValueChanged(object sender, EventArgs e)
        {
            T = (float)trackBar1.Value / 740;
            converter();
            wave_val.Text = Convert.ToString(trackBar1.Value);
        }

        private void Polarize_CheckedChanged(object sender, EventArgs e)
        {
            if (Polarize.Checked)
                IfVal.Text = Convert.ToString(Math.Round(sI * sI / 1.5f, 2));
            else
                IfVal.Text = Convert.ToString(Math.Round(PolI * PolI / 3.0f, 2));
        }

        //перерасчет амплитуд при изменении качества поляризаторов
        private void Quality_ValueChanged(object sender, EventArgs e)
        {
            pol_val.Text = Convert.ToString(Quality.Value);
            Y = (float)Math.Sqrt(1.5f * Quality.Value / 100);
            fI = (float)(Math.Sqrt(1.5f  * Quality.Value / 100) * Math.Abs(Math.Cos((float)FirstA.Value / 180 * Math.PI)));
            sI = (float)(fI * Math.Abs(Math.Cos((float)(SecondA.Value - FirstA.Value) / 180 * Math.PI)) * Math.Sqrt((float)Quality.Value / 100));
            PolI = (float)(Y * Math.Abs(Math.Cos((float)(SecondA.Value - FirstA.Value) / 180 * Math.PI)) * Math.Sqrt((float)Quality.Value / 100));
            if (Polarize.Checked)
                IfVal.Text = Convert.ToString(Math.Round(sI * sI / 1.5f, 2));
            else
                IfVal.Text = Convert.ToString(Math.Round(PolI * PolI / 3.0f, 2));
        }

        private void HButton_Click(object sender, EventArgs e)
        {
            Help.ShowHelp(this, "Help.chm", HelpNavigator.TableOfContents);
        }
    }
}
